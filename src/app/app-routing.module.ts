import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { LoginComponent } from './login/login.component';
import { StepsComponent } from './payment/steps/steps.component';

const routes: Routes = [
  // { path: 'login', component: LoginComponent },
  { path: 'steps', component: StepsComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
