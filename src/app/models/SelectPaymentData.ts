import { Question } from './Question';
export class SelectPaymentData {

    AmountQuestions: Question[];
    BillingStartDateLabel: string;
    CreditCardTypeList: number[];
    FrequencyInstructions: string;
    FrequencyTypeList: number[];
    MaximumBillingStartDate: string;
    MinimumBillingStartDate: string;
    PaymentInstructions: string;
    PaymentQuestion: string;
    PaymentTypeAmountLabel: string;
    PaymentTypeLabel: string;

    constructor(){
        this.AmountQuestions = [];
        this.BillingStartDateLabel = '';
        this.CreditCardTypeList = [];
        this.FrequencyInstructions = '';
        this.FrequencyTypeList = [];
        this.MaximumBillingStartDate = '';
        this.MinimumBillingStartDate = '';
        this.PaymentInstructions = '';
        this.PaymentQuestion = '';
        this.PaymentTypeAmountLabel = '';
        this.PaymentTypeLabel = '';

    }

}