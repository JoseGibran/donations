import { SelectPaymentData } from './SelectPaymentData';
export class SelectPaymentResponse{
    Data: SelectPaymentData;
    DiagnosticMessages: any;
    Errors: any;
    RecordCount: any;
    ValidationErrors: any;
}