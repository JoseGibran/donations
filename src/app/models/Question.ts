import { AmountOption } from "./AmountOption";

export class Question {
    AmountOptions: AmountOption[];
    FrequencyType: number;
    MinimumDonationAmount: number;
    OptionType: number;
    PaymentAmountType: number;
    QuestionText: string;
    
}