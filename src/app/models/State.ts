export class State {
    Code: number;
    Name: string;
    Abbreviation: string;
}