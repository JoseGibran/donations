export class Card {

    public constructor(id: number, name: string, mask: string, cardValidator: any){
        this.id = id;
        this.name = name;
        this.mask = mask;
        this.cardValidator = cardValidator;
    }

    id: number;
    name: string;
    mask: string;
    cardValidator: any;
    validate(number) {
      return this.cardValidator(number);
    }

   
}