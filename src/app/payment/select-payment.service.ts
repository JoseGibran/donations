import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { apiKey, urlBase, Globals } from '../shared/config';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { SelectPaymentResponse } from '../models/SelectPaymentResponse';

@Injectable()
export class SelectPaymentService {

  constructor(
    private http: HttpClient,
  ) {}

  getData() : Observable<SelectPaymentResponse>{
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Globals.appToken}`
      })
    };
    return this.http.get<SelectPaymentResponse>(`${urlBase}Configuration/PaymentTypeConfiguration?donorToken=${Globals.donorToken}&paymentType=2&apiKey=${apiKey}`, options).pipe(
      tap(response =>{}),
      catchError(this.handleError('login', null))
    )
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
     console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
