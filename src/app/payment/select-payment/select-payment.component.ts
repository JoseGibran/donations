import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { SelectPaymentService } from '../select-payment.service';
import { SelectPaymentData } from '../../models/SelectPaymentData';

@Component({
  selector: 'app-select-payment',
  templateUrl: './select-payment.component.html',
  styleUrls: ['./select-payment.component.css']
})
export class SelectPaymentComponent implements OnInit {
  public radioGroupForm: FormGroup;
  public frequencyGroupForm: FormGroup;
 

  public data : SelectPaymentData = new SelectPaymentData() ;
  
  constructor(
    private selectPaymentService : SelectPaymentService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.selectPaymentService.getData().subscribe(response => {
      if(response){
        this.data = response.Data;
      }else{
        
      }
    });
    this.radioGroupForm = this.formBuilder.group({
      'amount': ''
    });
    this.frequencyGroupForm = this.formBuilder.group({
      'frequency': '',
    });    
  }
  getFrequencyLabel(id) : string {
    switch(id){
      case 1: return "One-Time";
      case 2: return "Montly";
      case 3: return "Quaterly";
      case 4: return "Semi-annual";
    }
  }

  validateDonationData(){
    if(parseInt(this.radioGroupForm.value.amount) > 0){
      if(this.frequencyGroupForm.value.frequency !== ''){
        console.log("Success!");
      }else{
        alert("You must select a frequency!");
      }
     
    }else{
      alert("The Donation value can't be 0");
    }
  }

}
