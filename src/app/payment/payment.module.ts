import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask'

import { PaymentRouterModule } from './payment-routing.module';

import { AddressInformationComponent } from './address-information/address-information.component';
import { CardInformationComponent } from './card-information/card-information.component';
import { SelectPaymentComponent } from './select-payment/select-payment.component';
import { StepsComponent } from './steps/steps.component';

import { AddressInformationService } from './address-information.service';
import { SelectPaymentService } from './select-payment.service';

@NgModule({
    imports:[
        PaymentRouterModule,
        NgbModule.forRoot(),
        NgxMaskModule.forRoot(),
        ReactiveFormsModule,
        CommonModule,
    ],
    declarations: [
        AddressInformationComponent,
        CardInformationComponent,
        SelectPaymentComponent,
        StepsComponent
    ],
    providers: [
        AddressInformationService,
        SelectPaymentService,
    ]
})
export class PaymentModule { }