import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { AddressInformationService } from '../address-information.service';

import { Country } from '../../models/Country';
import { State } from '../../models/State';

@Component({
  selector: 'app-address-information',
  templateUrl: './address-information.component.html',
  styleUrls: ['./address-information.component.css']
})
export class AddressInformationComponent implements OnInit {

  public countries: Country[];
  public states: State[];

  public addressInformationForm: FormGroup;
  
  constructor(
    private addressInformationService : AddressInformationService
  ) { }

  ngOnInit() {
    this.addressInformationForm = new FormGroup({
      country: new FormControl('us', [Validators.required]),
      address1: new FormControl('', [Validators.required]),
      address2: new FormControl(''),
      city: new FormControl('',  [Validators.required]),
      state: new FormControl(0),
      province: new FormControl(''),
      postalCode: new FormControl(''),
      zip: new FormControl(''),
      zipExtension: new FormControl(''),
    });

    this.addressInformationService.getCountries().subscribe(response => {
      if(response){
        this.countries = response.Data;
      }
    });

    this.addressInformationService.getStates().subscribe(response => {
      if(response){
        this.states = response.Data;
      }
    });

  }

  submitAddress(){
    if(this.addressInformationForm.value.country == 'us'){
      if(this.addressInformationForm.value.zip.length == 0){
        alert("The ZIP is Required");
      }else{
        if(this.addressInformationForm.value.zip.length !== 5){
          alert(`The Zip must be 5 characters length`);
        }
      }
      if(this.addressInformationForm.value.zipExtension.length > 0 && this.addressInformationForm.value.zipExtension.length !== 4){
        alert("The ZIP Extension must be 4 characters length");
      }
    }else{
      if(this.addressInformationForm.value.postalCode.length == 0){
        alert("The Postal Code is Required");
      }else{
        if(this.addressInformationForm.value.postalCode.length !== 5){
          alert("The Postal Code must be 5 characters length");
        }
      }
    }
    console.log(this.addressInformationForm.value);
  }

}
