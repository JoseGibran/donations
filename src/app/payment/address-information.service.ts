import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { apiKey, urlBase, Globals } from '../shared/config';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';




@Injectable()
export class AddressInformationService {

  constructor(
    private http: HttpClient,
  ) { }


  getCountries() : Observable<any>{
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Globals.appToken}`
      })
    };    
    return this.http.get<any>(`${urlBase}Configuration/Countries?&apiKey=${apiKey}`, options).pipe(
      tap(response =>{}),
      catchError(this.handleError('countries', null))
    )
  }

  getStates(): Observable<any>{
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Globals.appToken}`
      })
    };
    return this.http.get<any>(`${urlBase}Configuration/USStates?&apiKey=${apiKey}`, options).pipe(
      tap(response =>{}),
      catchError(this.handleError('countries', null))
    )
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
     console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
