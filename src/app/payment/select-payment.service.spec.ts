import { TestBed, inject } from '@angular/core/testing';

import { SelectPaymentService } from './select-payment.service';

describe('SelectPaymentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectPaymentService]
    });
  });

  it('should be created', inject([SelectPaymentService], (service: SelectPaymentService) => {
    expect(service).toBeTruthy();
  }));
});
