import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css']
})
export class StepsComponent implements OnInit {

  public currentStep : Number = 0;

  constructor() { }

  ngOnInit() {
  }

  setCurrentStep (step){
    this.currentStep = step;
  }
  
  isActive(step){
    return step == this.currentStep;
  }

}
