import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { patternValidator } from '../../shared/pattern-validator';
import moment = require('moment');

import { Card } from '../../models/Card';

@Component({
  selector: 'app-card-information',
  templateUrl: './card-information.component.html',
  styleUrls: ['./card-information.component.css']
})
export class CardInformationComponent implements OnInit {

  public cardInformationForm: FormGroup;
  public cards: Card[];
  public errors: string[];

  @Input() CreditCardTypeList: number[];

  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.cardInformationForm = new FormGroup({
      cardType: new FormControl(1, [Validators.required]),
      cardNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(14),
      ])),
      cardVerificationNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(3),
      ])),
      nameOnCard: new FormControl('', Validators.required),
      expirationDate: new FormControl('', Validators.required),
      contactEmail: new FormControl('', [Validators.required, patternValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
    });
    this.cards = [
      new Card(1, "Visa", "0000-0000-0000-0000", this.luhnCardValidator),
      new Card(2, "Master Card", "0000-0000-0000-0000", this.luhnCardValidator),
      new Card(3, "American Express", "0000-0000-0000-000", this.luhnCardValidator),
      new Card(4, "Discover", "0000-0000-0000-0000", this.luhnCardValidator),
      new Card(5, "Diners Club", "0000-0000-0000-00", this.luhnCardValidator),
    ]
  }

  submitCardInformation(){

    if(moment().isAfter(moment(`${this.cardInformationForm.value.expirationDate.substring(0,2)}/${this.cardInformationForm.value.expirationDate.substring(2,4)}`, "MM/YY"))){
      alert("Invalid Card Date!");
      //this.errors.push("Invalid Card Date!");
    }
    if(!this.getCardById(this.cardInformationForm.value.cardType).validate(this.cardInformationForm.value.cardNumber)){
      alert("Invalid Card Number!");
    }

    if(this.cardInformationForm.value.nameOnCard.split(" ").length < 2){
      alert("Card Name Invalid");
    }
  }

  getCardById(id) : Card {
    let cardWillReturn = null;
    this.cards.forEach((card, index) => {
      if(card.id == id){
        cardWillReturn = card;
      }
    });
    return cardWillReturn;
  }

  luhnCardValidator(val) {
    let sum = 0;
    for (let i = 0; i < val.length; i++) {
        let intVal = parseInt(val.substr(i, 1));
        if (i % 2 == 0) {
            intVal *= 2;
            if (intVal > 9) {
                intVal = 1 + (intVal % 10);
            }
        }
        sum += intVal;
    }
    return (sum % 10) == 0;
  }


}
