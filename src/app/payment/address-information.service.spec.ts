import { TestBed, inject } from '@angular/core/testing';

import { AddressInformationService } from './address-information.service';

describe('AddressInformationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddressInformationService]
    });
  });

  it('should be created', inject([AddressInformationService], (service: AddressInformationService) => {
    expect(service).toBeTruthy();
  }));
});
