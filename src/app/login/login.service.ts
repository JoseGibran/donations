import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { apiKey, urlBase, username, password, Globals } from '../shared/config';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 
    'Content-Type': 'application/json',
    'Authorization': 'Basic dmlhcm86cEBzc3dvcmQ=',
  }),
};

@Injectable()
export class LoginService {

  

  constructor(
    private http: HttpClient,
  ) { }


  authenticateApp () : Observable<any>  {
    return this.http.get<any>(`${urlBase}Application/Authenticate?apikey=${apiKey}`,httpOptions).pipe(
      tap(appToken =>{
        Globals.appToken = appToken;
        console.log(Globals.appToken);
      }),
      catchError(this.handleError('authenticateApp', null))
    );
  }

  login (campaignCode, username, password) : Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${Globals.appToken}`
      })
    };
    return this.http.get<any>(`${urlBase}/Donor/Authenticate?campaignCode=${campaignCode}&username=${username}&password=${password}&apikey=${apiKey}`, options).pipe(
      tap(response =>{
        Globals.donorToken = response.Data.DonorToken;
      }),
      catchError(this.handleError('login', null))
    )
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
     console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
