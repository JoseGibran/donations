import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';

import { LoginService } from './login.service';

import { patternValidator } from '../shared/pattern-validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private loginService: LoginService, 
    private router: Router,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm(){
    this.loginForm = new FormGroup({
      campaignCode: new FormControl('30', [Validators.required]),
      username: new FormControl('MPEDonor30', [Validators.required]),
      password: new FormControl('MPEDonor30.', Validators.required),
    });
  }

  public login() {
    this.spinner.show();
    this.loginService.authenticateApp().subscribe(appToken=>{
      if(appToken){
        const { campaignCode, username, password } = this.loginForm.value;
        this.loginService.login(campaignCode, username, password).subscribe(response => {
          this.router.navigate(['steps']);
          this.spinner.hide();
        });
      }else{
        alert("Error at register application");
        this.spinner.hide();
      }
    });
  }

}
