import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

import { LoginRouterModule } from './login-routing.module';


import { LoginComponent } from './login.component';
import { LoginService } from './login.service';

@NgModule({
    imports:[
        LoginRouterModule,
        NgbModule.forRoot(),
        ReactiveFormsModule,
        CommonModule,
        NgxSpinnerModule.forRoot(),
    ],
    declarations: [
        LoginComponent
    ],
    providers: [
        LoginService
    ]
})
export class LoginModule { }